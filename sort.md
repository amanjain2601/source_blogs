# In this post we would be talking about how Sorting works on diffirent data types in JavaScript.

# Syntax of sort Function.
```js
sort() // without function

sort((element1,element2)=>{ }) //arrow function

sort(compareFunction) // Compare function passed inside which compare values

sort(function(element1,element2){ }) // inline function which compares
```

### Key points

- the Default sorting is in ascending order.
- if the compare function is omitted it will treat the values of array as string and sort them in ascending order according to Unicode of characters.
- sort works on Arrays.
- sorting takes inplace there is no copy of Array made it returns sorted array.
- All undefined elements are sorted to the end of array with no call to compare function.
</br>

### Compare Function 

| compareFunction(a,b) | sort order |
| -------------------- | ---------- |
| a - b > 0            |sort b before a|
| a - b < 0             |sort a before b|
| a- b ==0              |keeps original |

- It takes two parameters according to which the array is sorted

# How to sort strings.

- Sort the string array omitting compare function
```js
  let stringArray=["India","England","China","Thailand"];
  stringArray.sort();
  console.log(stringArray);
  // [ 'China', 'England', 'India', 'Thailand' ]
```

- Sort number array omitting compare function
```js
   let numberArray=[9000,8000,7];
   numberArray.sort();
   console.log(numberArray);
   // [ 7, 8000, 9000 ]
   // array elements are converted to string and then according to Unicode character value in ascending order
```

- Sort array containing objects with respect to string.
```js
   let object = [{ name: "yash", age: 34 },
              { name: "aman", age: 19 },
              { name: "pihu", age: 6 }
              ];

object.sort(function (a, b) {
  let x=a.name.toUpperCase();
  let y=b.name.toUpperCase();
  
  if(x>y)
  return 1;
  if(x<y)
  return -1;

  return 0;
})
console.log(object);
/*[ { name: 'aman', age: 19 },
  { name: 'pihu', age: 6 },
  { name: 'yash', age: 34 } ]*/
  //array of object sorted with respect to names in ascending order.
```
</br>

# How to sort numbers

- if number is sort without using comparator function it will be treated as strings and sort according to its unicode.

- Sort Numbers array in ascending and descending order.
```js
let numberArray = [9, 8, 789, 65, 81, 219, 77];

numberArray.sort(function (a, b) {
  return a - b;
})

console.log(numberArray);
//[ 8, 9, 65, 77, 81, 219, 789 ]
//Numbers are sorted in ascending order.

numberArray.sort(function (a, b) {
  return b - a;
})

console.log(numberArray);
//[ 789, 219, 81, 77, 65, 9, 8 ]
//Numbers sorted in Descending order.
```

- Sort Float Numbers
```js
  let numbers = [9.89, 6.76, 1.882, 4.5678, 3.09]
numbers.sort(function (a, b) {
  return a - b;
})
console.log(numbers);
// [ 1.882, 3.09, 4.5678, 6.76, 9.89 ]
``` 

- Sort object array with respect to age of person
```js
   let personData = [{ name: "Aman", age: "15Y" },
                  { name: "Vikas", age: "13Y" },
                  { name: "Anuj", age: "11Y" }
                 ];

personData.sort(function (a, b) {
  let age1=parseInt(a.age.slice(0, a.age.length - 1));
  let age2=parseInt(b.age.slice(0, b.age.length - 1));
 
  return age1 - age2;
})

console.log(personData);
/*[ { name: 'Anuj', age: '11Y' },
  { name: 'Vikas', age: '13Y' },
  { name: 'Aman', age: '15Y' } ]*/
```
</br>

# How to Sort Date

- sort array of date
```js
 let arr = ["2011-08-01T19:16:34.791Z", "2018-08-01T11:11:34.791Z", "2012-08-01T15:16:34.791Z"]
arr.sort(function (a, b) {
  return new Date(a) - new Date(b);
})

console.log(arr);
/* [ '2011-08-01T19:16:34.791Z',
     '2012-08-01T15:16:34.791Z',
     '2018-08-01T11:11:34.791Z' ] */
 ```

 - sort array of objects according to the date.
 ```js
   var array = [{id: 1, date:'Mar 12 2012 10:00:00 AM'}, {id: 2, date:'Mar 8 2012 08:00:00 AM'}];


array.sort(function(a, b) {
    var c = new Date(a.date);
    var d = new Date(b.date);
    return c-d;
});

console.log(array);
/*[ { id: 2, date: 'Mar 8 2012 08:00:00 AM' },
  { id: 1, date: 'Mar 12 2012 10:00:00 AM' } ] */
```

- Sort movies by release date
```js
  let movies = [{
  title: 'The Godfather',
  rating: 9.2,
  release: '24 March 1972'
},
{
  title: 'The Godfather: Part II',
  rating: 9.0,
  release: '20 December 1972'
},
{
  title: 'The Shawshank Redemption',
  rating: 9.3,
  release: '14 October 1994'
},
];

movies.sort(function (a, b) {
  let dateA = new Date(a.release);
  let dateB = new Date(b.release);
  return dateA - dateB;
});

console.log(movies);
/* [ { title: 'The Godfather', rating: 9.2, release: '24 March 1972' },
  { title: 'The Godfather: Part II',
    rating: 9,
    release: '20 December 1972' },
  { title: 'The Shawshank Redemption',
    rating: 9.3,
    release: '14 October 1994' } ] */
```
   

  








