# In this blog we would talk about how Chaining done with High Order Functions in JavaScript.

# What is Chaining ?
- Chaining is the process when one functions returns an object which is used by another function chained with it using a dot(".") operator.
-  High order Functions like map reduce and filter returns new array after performing some operations in callback functions iterating over the array passed to it.
-  The returned array from previous function is used for the function chained with previous function.  

# Why Chaining is Done ?
- Its helps to make code More Readable and short.
- Makes code cleaner.
</br>

# How Chaining Works ?
-  The array which is passed to one function after function returns the array the returned array is then passed to function which is chained to function using Dot operator
        
- #### *array.function1( ( )=>{ }) .function2( )=>{ } ).function3( ( )=>{ } ) and so on where all functions are high order functions*
</br>

find the sum of all even numbers in an array
```js
let number = [2, 79, 83, 91, 4, 96, 1000];

let SumOfEvenNumbers = number.filter((current) => {
    return current % 2 == 0;
}).reduce((sum, current) => {
    return sum + current;
}, 0)

console.log(SumOfEvenNumbers);
// 1102
```
**Notice with Chaining it makes code shorter otherwise we have to create extra variables to perform the task.**

find names of all people with age less than or equal to 18
```js
let personArray =
    [{ name: "rohan", age: 18 },
    { name: "ashish", age: 16 },
    { name: "satish", age: 17 },
    { name: "arjun", age: 40 },
    { name: "sam", age: 12 }];

let names = personArray.filter((currentObject) => {
    return currentObject.age <= 18
}).map((currentObject) => {
    return currentObject.name;
})

console.log(names);
// [ 'rohan', 'ashish', 'satish', 'sam' ]
```
find sum of salaries of all software developers.
```js
let personArray =
    [{ name: "rohan", age: 18, job: "software Developer", salary: 100000 },
    { name: "ashish", age: 16, job: "data analyst", salary: 20000 },
    { name: "satish", age: 17, job: "software Developer", salary: 200000 },
    { name: "arjun", age: 40, job: "tester", salary: 60000 },
    { name: "sam", age: 12, job: "manager", salary: 80000 },
    ]

let SalaryOfAllSoftwareDevelopre = personArray.filter((currentObject) => {
    return currentObject.job == "software Developer";
}).map((currentObject) => {
    return currentObject.salary;
}).reduce((sum, current) => {
    return sum + current;
}, 0)

console.log(SalaryOfAllSoftwareDevelopre);
// 300000
```
Catogorize all software developers with respect to age above or below 18
```js
let personArray =
    [{ name: "rohan", age: 28, job: "software Developer", salary: 100000 },
    { name: "ashish", age: 16, job: "data analyst", salary: 20000 },
    { name: "satish", age: 17, job: "software Developer", salary: 200000 },
    { name: "arjun", age: 40, job: "tester", salary: 60000 },
    { name: "sam", age: 12, job: "manager", salary: 80000 },
    { name: "anuj", age: 15, job: "software Developer", salary: 70000 }]

let catogorizeWithAge = personArray.filter((currentObject) => {
    return currentObject.job == "software Developer";
}).reduce((object, currentObject) => {
    if (currentObject.age <= 18) {
        if (object["Below_18"] == undefined) {
            object["Below_18"] = [];
            object["Below_18"].push(currentObject);
        }
        else
            object["Below_18"].push(currentObject);
    }
    else {
        if (object["Above_18"] == undefined) {
            object["Above_18"] = [];
            object["Above_18"].push(currentObject);
        }
        else
            object["Above_18"].push(currentObject);
    }

    return object;
}, {})

console.log(catogorizeWithAge);
/* { Above_18:
   [ { name: 'rohan',
       age: 28,
       job: 'software Developer',
       salary: 100000 } ],
  Below_18:
   [ { name: 'satish',
       age: 17,
       job: 'software Developer',
       salary: 200000 },
     { name: 'anuj',
       age: 15,
       job: 'software Developer',
       salary: 70000 } ] } */

```


  


